const express = require("express");
const userRoutes = express.Router();
const userController = require("../controller/userController");


userRoutes.post("/register", userController.Register);

userRoutes.post("/login",  userController.Login);

module.exports = userRoutes;