const express = require("express");
const productRoutes = express.Router();
const productController = require("../controller/productController");


productRoutes.post("/addProduct", productController.addProduct);

productRoutes.put("/updateProduct/:id",  productController.updateProduct);

productRoutes.get("/getProduct",  productController.getProducts);

productRoutes.delete("/deleteProduct/:id",  productController.deleteProduct);

module.exports = productRoutes;