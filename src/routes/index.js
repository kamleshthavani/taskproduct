const express = require("express");
const routes = express();
const userRoute = require("./userRoutes");
const productRoute = require("./productRoutes");


routes.use("/auth", userRoute);

routes.use("/product", productRoute);

module.exports = routes;