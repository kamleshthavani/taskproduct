const User = require("../models/user");

exports.Register  = async (req, res, next) => {

    try{    
        console.log("Register");
        const user = new User(req.body);

        await user.save((err, result) => {
            if(err) return res.json({ code: 2, message: "Register Unsuccesfull"});

            return res.json({
                code: 5,
                message: "Register Successfull."
            });
        })
    }   
    catch(e) {
        console.log(e);
        return res.json({ 
            code: 0,
            error: "There is some issue. Please try after some time. "
        })
    }
}


exports.Login  = async (req, res, next) => {

    try{    
        console.log("Login");
        const query = req.body;
        const user = await User.findOne({email: query.email}).lean();

        if(!user) 
            return res.json({ code: 2, message: "User does not exists"});
        
        if(password !== query.password)
            return res.json({ code: 2, message: "Wrong Password"});
        else {
            return res.json({
                code: 5,
                message: "Login Successfull",
                token: token
            })
        }
    }   
    catch(e) {
        console.log(e);
        return res.json({ 
            code: 0,
            error: "There is some issue. Please try after some time."
        })
    }
}