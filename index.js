const express = require("express");
const app = express();
const path =require("path");
const bodyParser =  require("body-parser");
const mongoose = require("mongoose");
const api = require("./src/routes");

//Connection Object
const connection = connect();

//Cross-origin setting
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, jwt");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
  next();
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//API calls
app.use("/api", api);


// //Front end
app.use(express.static(path.join(__dirname, "./www")));
app.get("*", (req, res) => {
  console.log(req.url);
  switch(req.url){
    case "/login":
      res.sendFile(path.join(__dirname, "./www/login.html"));
      break;
     case "/register":
      res.sendFile(path.join(__dirname, "./www/register.html"));
      break;
    default:
      res.sendFile(path.join(__dirname, "./www/index.html"));
  }
  
});

// Monoose Connection
connection
  .on("error", console.log)
  .on("disconnected", connect)
  .once("open", listen);

//Server Listen method
function listen() {
  if (app.get("env") === "test") return;
  app.listen(3000);
  console.log("Express app started on port 3000");
}

// Monoose Connection Method
function connect() {
  var options = { keepAlive: 1, useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true };
  mongoose.connect(`mongodb://localhost:27017/products`, options);
  return mongoose.connection;
}
