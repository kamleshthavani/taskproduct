$(document).ready(function(){
    
    $("#registerForm").submit(function(){
        var email = $("#email").val();
        var password = $("#pwd").val();

        $.post("/api/auth/register",
        {
            email: email,
            password: password
        },
        function(data, status){
            if(data.code === 5){
                 alert(data.message);
            window.location.reload();
            }
           
        });
    });

});